# Mapas Conceptuales

## *Computadoras Electrónicas*

```plantuml
@startmindmap

!theme black-knight

* **Computadoras Electrónicas**

** Computadoras electromecánicas
*** Harvard Mark I
****_ **Construida**
***** IBM En 1944
****_ **Tenia**
***** 765,000 componentes
***** 80 Km de cables
***** Eje de 15 metros que atravesaba toda la estructura\n para mantener en sincronía el dispositivo.
***** Un motor de 5 caballos de fuerza.
****_ **Uso**
***** Hacer simulaciones cálculos para el proyecto manhattan
******_ **Es**
******* El proyecto que desarrolló la bomba atómica
****_ **Operaciones**
***** Podia hacer 3 sumas o restas por segundo
***** Multiplicacion en 6 segundos
***** Division 15 Segundos
***** Operaciones complejas minutos
****_ **Relés**
***** Tenía aproximadamente 3500 relés 

***_ **Elemento Principal**
**** El Relé 
*****_ **Es**
****** es un interruptor mecánico controlado eléctricamente
*****_ **Compuesto por**
****** Una bobina
****** Contactos
****** Brazo de hierro
*****_ **Funciona**
****** cuando una corriente eléctrica atraviesa la bobina se genera un campo magnético
*****_ **Capacidad**
****** Cambiar de estado 50 veces por segundo
*****_ **Problemas**
****** Más susceptible al daño por el uso y por el tiempo
****** Mientras más reales existan mayor es la posibilidad de avería

***_ **Bugs**
**** Cuando algo salía mal con una computadora decíamos que tenían bichos y fue así como nació

** Computadoras Electrónicas
***_ **Alternativa a los relés**
**** Válvula termo iónica
*****_ **Inventada En**
****** 1904 Por Joh Fleming
*****_ **Formada por**
****** Filamento
****** Ánodo y Cátodo
****** Bulbo de cristal
*****_ **Funciona**
****** cuando el filamento se enciende y se calienta permite el flujo de electrones \ndesde el ánodo hacia el cátodo este proceso es llamado emisión térmica \ny permite el flujo de la corriente en una sola dirección.

***_ **1906**
**** el inventor americano Lee De Forest agregó \nun tercer electrodo llamado: electrodo de control
*****_ **Funcion**
****** Al aplicar una carga positiva o negativa en el electrodo de control permite \nel flujo o detienen la corriente respectivamente.
*****_ **Mejoras**
****** Se dañaban menos en el uso
****** Mayor velocidad
*****_ **Contras**
****** Eran frágiles
****** Se podían quemar como los focos o bulbos de luz
*****_ **Capacidad**
****** Cambiar de estado miles de veces por segundo

*** Colossus Mark I
****_ **Diseñada Por **
***** El Ing. Tommy Flowers
****_ **Finalizada**
***** Diciembre de 1943
****_ **Instalada en**
***** Bletchley Park en Reino Unido
****_ **Tenia**
***** 1,600 Tubos de vacio
****_ **Conocida Como**
***** La primera computadora electrónica programable

*** ENIAC
****_ **Siglas**
***** Calculadora
***** Integradora
***** Numerica
***** Electronica
****_ **Construida**
***** En 1946 en la universidad de pensilvania
****_ **Diseñada por**
***** John Mauchly
***** J. Presper Eckert
****_ **Capacidad**
***** Podia realizar 5000 dumas o restas de 10 digitos por segundo.

*** Bell Laboratories En 1947
****_ **Crearon**
***** El transistor
******_ **Por**
******* Joh Bardeen 
******* Walter Brattain
******_ **Funcion**
******* Cumple la misma función que un relé o un tubo de\n vacío es decir un interruptor eléctrico.
******_ **Compuesto por**
******* Un elemento el llamado silicio
******_ **Capacidad**
******* Podia cambiar de estado 10mil veces por segundo
******_ **Ventajas**
******* Muchos más resistentes
******* Mucho más pequeños de lo que los relés

*** IBM 608
****_ **Lanzada**
***** 1957
****_ **Basada En**
***** Transistores
****_ **Capacidad**
***** 4500 Sumas por minuto
***** 80 divisiones o multiplicaiones por minuto

*** Hoy en día los transistores
****_ **Tamaño**
***** Menor a 50 Nanometros
****_ **Capacidad**
***** Cambiar de estados millones de veces por segundo

@endmindmap
```

## *Arquitectura Von Neumann y Arquitectura Harvard*
```plantuml
@startmindmap
!theme hacker

* **Arquitectura Von Neumann y Arquitectura Harvard**

** Arquitectura Von Neumann
***_ Partes Fundamentales
**** CPU
*****_ Contiene
****** Controlador de programa
****** Registro de instruccion
****** Registro de direccion de memoria
****** Registro de buffer de memoria
****** Registro de direccion de E/S
****** Registro de buffer E/S
**** Memoria Principal
*****_ Contiene
****** Datos
****** Instruccion
**** Modulo de E/S
*****_ Contiene
****** Registros
***_ Todo conectado por
**** Bus Del Sistema
***_ Modelo
**** Es una arquitectura basada en la descrita en 1945 por el matematico y fisico Jonh Von Neumann\n y otros. En el primer borrador de un informe sobre el EDVAC.
***_ Modelo de buses
**** Bus de datos
**** Bus de direcciones
**** Bus de control
***_ Ciclo De Ejecucion
**** Fetch
**** Decode
**** Execute

** Arquitectura Harvard
***_ Modelo
**** Utilizaban dispositivos de almacenamiento fisicamente \nseparados para las instrucciones y para los datos.
***_ Contiene
**** CPU
***** Unidad de control
***** unidad aritmetica y logica
**** Memoria
***** Memoria de instrucciones
***** Memoria de datos
**** Sistema De E/S
***_ Memorias
**** Memoria Cache
***** La solucion, por tanto, es proporcionar una pequeña contidad\n de memoria muy rapida conocida como CACHE.
***_ Solucion Harvard
**** Las instrucciones y los datos se almacenan en caches\nseparados para mejorar el rendimiento.
*** Microcrontrolador
****_ Donde se utilizan
***** Electrodomesticos
***** telecomunicaciones
***** automoviles
***** impresoras

@endmindmap
```
## *Basura Electrónica*
```plantuml
@startmindmap
!theme silver

* Basura Electrónica
**_ Elementos Que Se Extraen
*** Oro
*** cobre
*** plata
*** cobalto
**_ Exportada a paises
*** China
*** India
*** Mexico
**_ Mercado Ilegal
*** Reciclar los electrónicos de manera segura es un proceso costoso y complicadísimo
**_ Contaminacion
*** los residuos electrónicos quemados contaminan el agua y el aire.
**_ Problemas de salud
*** todo el polvo llena pulmones y da tos fiebre o dolores articulares\nque pueden convertirse en tuberculosis y otras enfermedades.
**_ Actividades Criminales
*** información personal que puedan usar para robar la identidad de una persona
**** borrar la información es obviamente más importante que el reciclaje\nporque esto protege la identidad de la gente

**_ RAEE
*** los residuos y aparatos eléctricos y electrónicos, son aquellos aparatos que tenemos en casa \ncomo una licuadora, un televisor, una computadora, un celular culmina su vida útil
****_ Son
***** metales
***** plasticos
***** otros compuestos
****_ gestion de residuos
***** Recoleccion
***** Seleccionados por categoria
***** ser desarmados para recuperar los materiales primarios
***** reintroducirlos a una nueva cadena productiva
****_ Material Peligroso
***** tiene un tratamiento diferente 
****_ Material Reciclable
***** fundamentalmente plásticos metales y tarjetas electrónicas

**_ Reciclaje de desechos Eectrónicos
*** le recibimos un material, les decimos cómo se clasifica y les decimos cómo le vamos a pagar\n cada una de sus tarjetas, lógicamente le pedimos su identificación lo que es su credencial\n para generarle un archivo y tener un mejor trato

@endmindmap
```